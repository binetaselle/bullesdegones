<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'w@:P[h`}`Gnz]f|2b7yyHTOI(F>!5&;GVw=O@A}f8j`KHb!?y@oMQKJ=RU!UjoSY' );
define( 'SECURE_AUTH_KEY',  ' OPx-9IcjQ%eE7IpY2USSyR^x.9N7;sCZ;@kZMGA@Gy-3WGQb8EUoOt-NuIU8B!Z' );
define( 'LOGGED_IN_KEY',    'jk!7#f_K)T>lpK%_gVn#KaZkp>I(>x^Meplx0?t?Jz=vVw&xY{3B+vB$A0Oi_rxz' );
define( 'NONCE_KEY',        ')9WdA<i}=+jB*gDle0cN},+ zq=~J~1$Lt.%h=D(|t+4=NA&s+Xs8yD)_X[0q[um' );
define( 'AUTH_SALT',        'iG]mi8ZaUN@s>-ZEq!FWH+/<5aRpJjI~9;!*klW!AxUFk%t)&qs_PA0)Wzj)[O8J' );
define( 'SECURE_AUTH_SALT', '>*4&`;R)_}wGAfs[V/_}5Af}<E 3Sl|n)]b!HY`#ATuGf)$0B]ggyy;1y:)jr#i,' );
define( 'LOGGED_IN_SALT',   'YQn18B[GAOCDT-wCMlP!zxgT2Yws[gcu_ZM@p18L%S%>`U:;{~steE;HEiz<d3Vu' );
define( 'NONCE_SALT',       'i|ntqJ-5+H1&]L._@6F:U05CB5YgHgCcA7r}-gN|9c22t)!vqN=wmiH9XGH]9P+)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
